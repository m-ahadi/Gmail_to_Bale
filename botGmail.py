import re
import imaplib
import email
import asyncio
import pprint
import base64
from balebot.filters import *
from balebot.models.base_models import Peer
from balebot.models.messages import *
from balebot.updater import Updater


# Time interval to check email in seconds
time_to_check=60*10
gmail_user = 'your_email@gmail.com'
gmail_pwd = 'your password'  # if you have two step authentication you should make a app password for this
user_peer = Peer(peer_type="User", peer_id=your_bale_ID, access_hash="your accesshash")
updater = Updater(token="your bot token")


server = "imap.gmail.com"
port_num = 993
conn = imaplib.IMAP4_SSL(server, port_num)




btn_list = [TemplateMessageButton(text="مشاهده آخرین ایمیل خوانده نشده", value="last", action=0)]



bot = updater.bot
dispatcher = updater.dispatcher


def success(response, user_data):
    # Check if there is any user data or not
    if user_data:
        user_data = user_data['kwargs']
        user_peer = user_data["user_peer"]
        message = user_data["message"]
        print("message : " + message.text + "\nuser id : ", user_peer.peer_id)
    print("success : ", response)


def failure(response, user_data):
    print("user_data : ", user_data)
    print("failure : ", response)


async def read_email1(gmail_user, gmail_pwd, send_note):
    global message_ids
    global conn
    conn = imaplib.IMAP4_SSL(server, port_num)
    conn.login(gmail_user, gmail_pwd)
    conn.select()

    # Check status for 'OK'
    status, all_folders = conn.list()

    folder_to_search = 'INBOX'
    # folder_to_search = '[Google Mail]/All Mail'

    # Check status for 'OK'
    status, select_info = conn.select(folder_to_search)

    if status == 'OK':

        # #  Search for relevant messages
        # #  see http://tools.ietf.org/html/rfc3501# section-6.4.5

        search_key = "is:unread"
        status, message_ids = conn.search(None, 'X-GM-RAW', search_key)
        if len(message_ids[0].split()) > 0 and send_note:
            print(''.join(["You have ", str(len(message_ids[0].split())), " unread email"]) )
            print("sending message")
            message = TextMessage(' '.join(["شما", str(len(message_ids[0].split())), "پیام خوانده نشده دارید."]))
            template_message = TemplateMessage(general_message=message, btn_list=btn_list)
            bot.send_message(template_message, user_peer, success_callback=success, failure_callback=failure)
            print("message sent")

    else:
        print("Could not login")
        message = TextMessage("عدم امکان اتصال به ایمیل!")
        bot.send_message(message, user_peer, success_callback=success, failure_callback=failure)


@dispatcher.message_handler([TemplateResponseFilter(keywords=["last"]),TextFilter(keywords=["Last","Next","last","next","آخرین","اخرین","بعدی"])])
def text_received(bot, update):
    read_next_mail()


@dispatcher.message_handler(TextFilter(keywords=["Myid","myid"]))
def myID(bot, update):
    user=update.get_effective_user().get_json_object()
    message = TextMessage(' '.join(["Your ID:",user["id"],'\nYour accessHash: ',user["accessHash"]]))
    bot.send_message(message, update.get_effective_user(), success_callback=success, failure_callback=failure)


def read_next_mail():
    global message_ids
    global conn
    status, all_folders = conn.list()
    folder_to_search = 'INBOX'
    status, select_info = conn.select(folder_to_search)
    if status == 'OK':
        print("Loged in")
        search_key = "is:unread"
        status, message_ids = conn.search(None, 'X-GM-RAW', search_key)
    else:
        print("Could not login")
        message = TextMessage("عدم امکان اتصال به ایمیل!")
        bot.send_message(message, user_peer, success_callback=success, failure_callback=failure)

    id = message_ids[0].split()
    if len(id) > 0:
        status, data = conn.fetch(id[-1], '(RFC822)')
        email_msg = email.message_from_string(data[0][1].decode("utf-8"))
        # pprint.pprint(data)
        # Print all the Attributes of email message like Subject,
        # print(email_msg.keys())

        subject = email_msg['Subject']
        if subject[0:10] == "=?UTF-8?B?":
            subject = base64.b64decode(subject.replace("=?UTF-8?B?", '')).decode("utf-8")
        sender_email = email_msg['From']
        sent_to_email = email_msg['To']
        print(sender_email)
        print(subject)

        email_content = ""
        for part in email_msg.walk():
            part.get_payload()
            if part.get_content_type() == 'text/html':
                if "Content-Transfer-Encoding: base64" in data[0][1].decode("utf-8"):
                    email_content = base64.b64decode(part.get_payload()).decode("utf-8")
                else:
                    email_content = part.get_payload()  # prints the raw text

                print(email_content)

        cleantext = re.sub("<.*?>", "", email_content)
        message = TextMessage(
            ' '.join(["*فرستنده :*", str(sender_email), "\n*عنوان :*", str(subject), "\n*متن :*", cleantext]))
        bot.send_message(message, user_peer, success_callback=success, failure_callback=failure)
        # updater._run_dispatcher()

        print("email sent")
        if len(message_ids[0].split())>1:
            message = TextMessage(' '.join(["شما", str(len(message_ids[0].split())-1), "پیام خوانده نشده دارید."]))
            template_message = TemplateMessage(general_message=message, btn_list=btn_list)
            bot.send_message(template_message, user_peer, success_callback=success, failure_callback=failure)
        else:

            message = TextMessage("تمام ایمیل ها خوانده شده است.")
            bot.send_message(message, user_peer, success_callback=success, failure_callback=failure)
    else:
        message = TextMessage("تمام ایمیل ها خوانده شده است.")
        bot.send_message(message, user_peer, success_callback=success, failure_callback=failure)



async def CheckMail():
    print("Checking emails...")
    await read_email1(gmail_user, gmail_pwd,True)
    await asyncio.sleep(time_to_check)
    await CheckMail()


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    tasks = [asyncio.ensure_future(updater.run()),
             asyncio.ensure_future(CheckMail())]
    loop.run_until_complete(asyncio.wait(*tasks))


