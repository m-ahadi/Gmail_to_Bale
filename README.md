# Gmail_to_Bale

<div dir="auto">
<p dir='rtl' align='right'>این برنامه با اتصال به ایمیل شما هر زمان که ایمیلی دریافت کنید از طریق پیام رسان بله به اطلاعتان می رساند.</p>
<p dir='rtl' align='right'>
این برنامه با استفاده از بات ساخته شده توسط کاربر در بازه های زمانی قابل تنظیم ایمیل های خوانده نشده را از طریق پیام رسان بله برای کاربر ارسال میکند.
</p>


<p dir='rtl' align='right'>
برای دریافت اطلاعات بیشتر میتوانید به این آدرس مراجعه کنید.
</p>

https://developers.bale.ai/

https://github.com/balemessenger/bale-bot-samples

<p dir='rtl' align='right'>
برای استفاده از این برنامه باید تنظیمات زیر را بر اساس بات و مشخصات شناسه کاربری خود در تنظیم کنید.
</p>

Gmail user & password <p></p>
Your Bale ID<p></p>
Your Bale accessHash<p></p>
Your bot token<p></p>

<p dir='rtl' align='right'>
برای بدست آوردن ID و accessHash برنامه را اجرا کرده و پیغام myid را به بات ارسال کنید.  پارامترهای دریافتی را در برنامه وارد کنید.
</p>


</div>
